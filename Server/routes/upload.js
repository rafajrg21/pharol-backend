const express = require('express');
const router = express.Router();
const app = express();

const uploadService = require('../services/UploadService');
const upload = require('../services/MulterConfig');

router.post('/upload', upload.any(), uploadService.createApp);

app.use(function(err,req,res,next) {
  if(err.code === "LIMIT_FILE_TYPES"){
    res.status(422).json({ error: "Solo se permite la subida de imagenes" })
    return;
  }

  if(err.code === "LIMIT_FILE_SIZE"){
    res.status(422).json({ error: "El archivo es demasiado grande" })
    return;
  }
});

module.exports = router;
