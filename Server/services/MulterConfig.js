// Upload Image Service 
const fs = require('fs');
const multer = require('multer');

const MAX_SIZE = 2000000;
const fileFilter = function (req, file, cb) {
    const allowedTypes = ["image/jpeg", "image/png", "image/gif"];

    if (!allowedTypes.includes(file.mimetype)) {
        const error = new Error("El archivo no es una imagen");
        error.code = "LIMIT_FILE_TYPES";
        return cb(error, false);
    }

    cb(null, true);
}

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        let path = `./server/public/img/${file.originalname}`; 
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
            cb(null, path);
        }
        cb(null, path);
    },
    filename: (req, file, cb) => {
        console.log(file)
        let filetype = '';
        if (file.mimetype === 'image/gif') {
            filetype = 'gif';
        }
        if (file.mimetype === 'image/png') {
            filetype = 'png';
        }
        if (file.mimetype === 'image/jpeg') {
            filetype = 'jpg';
        }
        cb(null, `image-${Date.now()}.${filetype}`);
    }
});

const upload = multer({
    storage: storage,
    fileFilter,
    limits: {
        fileSize: MAX_SIZE
    }
});

module.exports = upload