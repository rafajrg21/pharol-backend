//IMPORT THE MODEL WE CREATED EARLIER
const imageModel = require('../models/Images');

//IMPORT CLOUDINARY CONFIG HERE
const cloud = require('../cloudinaryConfig');

const createApp = (req, res) => {
    try {
        let imageDetails = {
            imageName: req.body.imageName,
        }

        //USING MONGO METHOD TO FIND IF IMAGE-NAME EXIST IN THE DB
        imageModel.find({
            imageName: imageDetails.imageName
        }, (err, callback) => {
            //CHECKING IF ERROR OCCURRED      
            if (err) {
                res.json({
                    err: err,
                    message: 'Hubo un problema subiendo la imagen'
                })
            } else if (callback.length >= 1) {
                res.json({
                    message: 'El archivo ya existe'
                })
            } else {
                let imageDetails = {
                    imageName: req.body.imageName,
                    cloudImage: req.files[0].path,
                    imageId: ''
                }
                // IF ALL THING GO WELL, POST THE IMAGE TO CLOUDINARY
                cloud.uploads(imageDetails.cloudImage).then((result) => {
                    let imageDetails = {
                        imageName: req.body.imageName,
                        cloudImage: result.url,
                        imageId: result.id
                    }
                    //THEN CREATE THE FILE IN THE DATABASE
                    imageModel.create(imageDetails, (err, created) => {
                        if (err) {
                            res.json({
                                err: err,
                                message: 'No se pudo subir la imagen, intente de nuevo'
                            })
                        } else {
                            res.json({
                                created: created,
                                message: "La imagen ha sido subida con exito!!"
                            })
                        }
                    })
                })
            }
        });
    } catch (execptions) {
        console.log(execptions)
    }
}

module.exports = {createApp};